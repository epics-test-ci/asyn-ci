REMOTE_GIT=https://github.com/epics-modules/

clone() {
  git clone "$REMOTE_GIT/$1" -b "$2" --recursive "$1"
}

configure() {
  source config.sh
  m4 -D_EPICS_BASE="$EPICS_BASE" release.m4 >"$1"/configure/RELEASE
  m4 -D_CROSS_COMPILER_TARGET_ARCHS="$CROSS_COMPILER_TARGET_ARCHS" config_site.m4 >"$1"/configure/CONFIG_SITE.local
}

build() {
  make -C "$1"
}

test -d asyn || clone asyn R4-42
configure asyn
build asyn
